#### README

- Install go-lang:

```
wget https://storage.googleapis.com/golang/go1.7.1.linux-amd64.tar.gz
sudo tar -zxvf go1.7.1.linux-amd64.tar.gz -C /usr/lib/
sudo ln -s /usr/lib/go/bin/go /usr/bin/go
sudo ln -s /usr/lib/go/bin/gofmt /usr/bin/gofmt
```

- Create go path varible in your system home

```

export GOARCH="amd64"
export GOBIN="/usr/bin/go"
export GOHOSTARCH="amd64"
export GOHOSTOS="linux"
export GOOS="linux"
export GOROOT="/usr/lib/go"
export GOTOOLDIR="/usr/lib/go/pkg/tool/linux_amd64"
export GO15VENDOREXPERIMENT="1"
export CC="gcc"
export GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0"
export CXX="g++"
export CGO_ENABLED="1"
export GOPATH=$HOME/go
mkdir $HOME/go

```